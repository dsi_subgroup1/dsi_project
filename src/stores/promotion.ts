import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'

import type { Promotion } from '@/types/Promotion'
import { useReceiptStore } from './receipt'

export const usePromotionStore = defineStore('promotion', () => {
  const promotionDatas = ref<Promotion[]>([
    {
      id: 1,
      name: '10 Point Get 1 Free',
      startDate: '2024-01-01',
      endDate: '2124-01-01',
      usePoint:-10,
      status: 1,
      discount:0
    },
    {
      id: 2,
      name: '20 Point Get 10% Discount',
      startDate: '2024-01-01',
      endDate: '2124-01-01',
      usePoint:-20,
      status: 1,
      discount: 0.9
    }
  ])

  const promotionDialog = ref(false)
  const canUsePromotion =ref(false)
  const promotionSelected = ref('')
  const usePromotion = ref(false)
  const discount = ref<number>(1)
  const pointDelete = ref(0)

  watch(promotionSelected,()=>{
    for(const item of promotionDatas.value){
      if(item.name===promotionSelected.value){
        discount.value = item.discount
        pointDelete.value = item.usePoint

      }
    }
  })
  const receiptStore = useReceiptStore()
  

  return { promotionDatas, promotionDialog, promotionSelected,discount ,pointDelete,usePromotion,canUsePromotion}
})
