import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useReceiptStore } from './receipt'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    {id:1 ,name:'สวัสดีครับ อาจารย์',tel: '0881234567',point:10},
    {id:2 ,name:'อาจารย์ กบใจดี',tel: '0887654321',point:99},
    {id:3 ,name:'ทดลองชิม',tel: '1',point:44}
  ])
  const receiptStroe = useReceiptStore()
  const currentMember = ref<Member | null>()
  const searchMember = (tel:string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if(index<0) {
        currentMember.value = null
    }
    currentMember.value = members.value[index]
    receiptStroe.receipt.member=members.value[index]
    receiptStroe.receipt.memberId = members.value[index].id

    
  }
  function clear(){
    
    currentMember.value = null
    
  }

  

  const memDialog = ref(false)

  function newMember() {

    memDialog.value=true
  }

  const createMemberNew = (newMember:Member)=>{
    members.value.push(newMember)
  }
  return { 
    members,currentMember,memDialog,newMember,
    searchMember,clear,createMemberNew
    }

    
})
