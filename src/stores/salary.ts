import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Salary } from '@/types/Salary'

export const useSalaryStore = defineStore('salary', () => {

    const salaries = ref<Salary[]>([
        {
            id: 1,
            email: 'Wanwadee@gmail.com',
            password: 'Pass@Word',
            fullName: 'Wanwadee Noijaroen',
            roles: ['Manager'],
            salary: '20000',
            status: 'Not Paid',
            date: '2024-01-01'
        },
        {
            id: 2,
            email: 'Nawapol@gmail.com',
            password: 'Pass@Word',
            fullName: 'Nawapol Kotechai',
            roles: ['Employee'],
            salary: '14000',
            status: 'Not Paid',
            date: '2024-01-01'
        },
        {
            id: 3,
            email: 'Tanakorn@gmail.com',
            password: 'Pass@Word1',
            fullName: 'Tanakorn Pummala',
            roles: ['Employee'],
            salary: '14000',
            status: 'Not Paid',
            date: '2024-01-01'
        },
        {
            id: 4,
            email: 'Tanakorn@gmail.com',
            password: 'Pass@Word1',
            fullName: 'Surapitch Kongthong',
            roles: ['Employee'],
            salary: '13000',
            status: 'Not Paid',
            date: '2024-01-01'
        },
        {
            id: 5,
            email: 'Peerada@gmail.com',
            password: 'Pass@Word1',
            fullName: 'Peerada Wangyaichim',
            roles: ['Employee'],
            salary: '14000',
            status: 'Not Paid',
            date: '2024-01-01'
        },
        {
            id: 6,
            email: 'User1',
            password: 'user1',
            fullName: 'User1',
            roles: ['Employee'],
            salary: '14000',
            status: 'Not Paid',
            date: '2024-01-01'
        }
    ])

    const defaultSalary: Salary = {
        id: 1,
        email: 'Wanwadee@gmail.com',
        password: 'Pass@Word',
        fullName: 'Wanwadee Noijaroen',
        roles: ['Manager'],
        salary: '20000',
        status: 'Not Paid',
        date: '2024-01-01'
    };

    return { salaries, defaultSalary }
})
