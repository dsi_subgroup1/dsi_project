import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {

    const users = ref<User[]>([
        {
            id: 1,
            email: 'Wanwadee@gmail.com',
            password: 'Pass@Word',
            fullName: 'Wanwadee Noijaroen',
            roles: ['Manager'],
            salary: '20000'
        },
        {
            id: 2,
            email: 'Nawapol@gmail.com',
            password: 'Pass@Word',
            fullName: 'Nawapol Kotechai',
            roles: ['Employee'],
            salary: '14000'
        },
        {
            id: 3,
            email: 'Tanakorn@gmail.com',
            password: 'Pass@Word1',
            fullName: 'Tanakorn Pummala',
            roles: ['Employee'],
            salary: '14000'
        },
        {
            id: 4,
            email: 'Surapitch@gmail.com',
            password: 'Pass@Word1',
            fullName: 'Surapitch Kongthong',
            roles: ['Employee'],
            salary: '13000'
        },
        {
            id: 5,
            email: 'Peerada@gmail.com',
            password: 'Pass@Word1',
            fullName: 'Peerada Wangyaichim',
            roles: ['Employee'],
            salary: '14000',
        },
        {
            id: 6,
            email: 'User1',
            password: 'user1',
            fullName: 'User1',
            roles: ['Employee'],
            salary: '14000',
        }
    ])

    const defaultUser: User = {
        id: 1,
        email: 'Wanwadee@gmail.com',
        password: 'Pass@Word',
        fullName: 'Wanwadee Noijaroen',
        roles: ['Manager'],
        salary: '20000',
    };

    const registerUser = async (user: any) => {
        // ทำการสมัครสมาชิก โดยเพิ่ม user ลงใน users array
        users.value.push(user);
        return user;
    };

    return { users, defaultUser, registerUser }
})
