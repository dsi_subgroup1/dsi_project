import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { ImportMaterial } from '@/types/ImportMaterial'
export const useImportMaterialStore = defineStore('importmaterial', () => {
    
const importmaterials = ref<ImportMaterial[]>([
    {
        id: 1,
        employee: 'Wanwadee Noijaroen',
        vendor: 'KShop',
        Rdate: '2024-01-01',
        Rtotallist: 0,
        Rtotal: 9420.00,
        details: [{materialid: 1, no: 1, material: 'Coffee Beans', exDate: '2024-01-30', quantity: 20, unitsprice: 400 ,price: 8000}, {materialid: 5, no: 2, material: 'Thaitea Powder', exDate: '2024/01/30', quantity: 15, unitsprice: 230 ,price: 3450}]
    },
    {
        id: 2,
        employee: 'Wanwadee Noijaroen',
        vendor: 'KShop',
        Rdate: '2024-02-01',
        Rtotallist: 0,
        Rtotal: 15550.00,
        details: [{materialid: 1, no: 1, material: 'Coffee Beans', exDate: '2024-01-30', quantity: 20, unitsprice: 400 ,price: 8000}]
    },
    {
        id: 3,
        employee: 'Surapitch Kongthong',
        vendor: 'LoShop',
        Rdate: '2024-03-01',
        Rtotallist: 0,
        Rtotal: 10805.00,
        details: [{materialid: 1, no: 1, material: 'Coffee Beans', exDate: '2024-01-30', quantity: 20, unitsprice: 400 ,price: 8000}]
    },
    {
        id: 4,
        employee: 'Nawapol Kotechai',
        vendor: 'LoShop',
        Rdate: '2024-03-20',
        Rtotallist: 0,
        Rtotal: 12500.00,
        details: [{materialid: 1, no: 1, material: 'Coffee Beans', exDate: '2024-01-30', quantity: 20, unitsprice: 400 ,price: 8000}]
    },
    {
        id: 5,
        employee: 'Peerada Wangyaichim',
        vendor: 'KShop',
        Rdate: '2024-04-05',
        Rtotallist: 0,
        Rtotal: 9875.00,
        details: [{materialid: 1, no: 1, material: 'Coffee Beans', exDate: '2024-01-30', quantity: 20, unitsprice: 400 ,price: 8000}]
    }
    
])
function TotalList() {
    importmaterials.value.forEach((importmaterials) => {
        importmaterials.Rtotallist = importmaterials.details.length;
    })
}

function TotalPrice() {
    importmaterials.value.forEach((importmaterials) => {
        importmaterials.Rtotal = importmaterials.details.reduce((total, detail) => {
            return total + detail.unitsprice * detail.quantity;
        },
            0)
    })
}

TotalList()
TotalPrice()

    return {importmaterials}
})