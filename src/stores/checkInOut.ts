import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { CheckInOut } from '@/types/CheckInOut'
import { useUserStore } from './user'
import { useAuthStore } from './auth'

export const useCheckInOutStore = defineStore('checkInOut', () => {
  const userStore = useUserStore()
  const authStore = useAuthStore()

  const d: CheckInOut = {
    id: 0,
    user: '',
    userId: 0,
    chkIn: '',
    chkOut: '',
    totalTime: '',
    inTime: 0,
    outTime: 0
  }
  const chkData = ref<CheckInOut[]>([
    {
      id: 1,
      userId: 1,
      user: 'Wanwadee Noijaroen',
      chkIn: '14/01/2024 08:00:00 น.',
      chkOut: '14/01/2024 16:00:00 น.',
      totalTime: '8 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 2,
      userId: 2,
      user: 'Nawapol Kotechai',
      chkIn: '14/01/2024 08:00:00 น.',
      chkOut: '14/01/2024 16:00:00 น.',
      totalTime: '8 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 3,
      userId: 2,
      user: 'Nawapol Kotechai',
      chkIn: '14/01/2024 08:30:00 น.',
      chkOut: '14/01/2024 16:30:00 น.',
      totalTime: '8 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 4,
      userId: 3,
      user: 'Tanakorn Pummala',
      chkIn: '14/01/2024 07:00:00 น.',
      chkOut: '14/01/2024 16:00:00 น.',
      totalTime: '9 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 5,
      userId: 3,
      user: 'Tanakorn Pummala',
      chkIn: '14/01/2024 08:00:00 น.',
      chkOut: '14/01/2024 16:00:00 น.',
      totalTime: '8 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 6,
      userId: 3,
      user: 'Tanakorn Pummala',
      chkIn: '14/01/2024 09:00:00 น.',
      chkOut: '14/01/2024 16:00:00 น.',
      totalTime: '7 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 7,
      userId: 6,
      user: 'User1',
      chkIn: '14/01/2024 11:00:00 น.',
      chkOut: '14/01/2024 16:00:00 น.',
      totalTime: '5 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 8,
      userId: 6,
      user: 'User1',
      chkIn: '14/01/2024 10:00:00 น.',
      chkOut: '14/01/2024 16:00:00 น.',
      totalTime: '6 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 9,
      userId: 6,
      user: 'User1',
      chkIn: '14/01/2024 06:00:00 น.',
      chkOut: '14/01/2024 11:00:00 น.',
      totalTime: '7 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 10,
      userId: 6,
      user: 'User1',
      chkIn: '14/01/2024 08:00:00 น.',
      chkOut: '14/01/2024 16:00:00 น.',
      totalTime: '8 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 11,
      userId: 4,
      user: 'Surapitch Kongthong',
      chkIn: '14/01/2024 15:00:00 น.',
      chkOut: '14/01/2024 16:00:00 น.',
      totalTime: '1 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    },
    {
      id: 12,
      userId: 5,
      user: 'Peerada Wangyaichim',
      chkIn: '14/01/2024 08:00:00 น.',
      chkOut: '15/01/2024 08:00:00 น.',
      totalTime: '24 ชั่วโมง 0 นาที่',
      inTime: 0,
      outTime: 0
    }
  ])

  function formatTime(date: Date): string {
    const day = date.getDate().toString().padStart(2, '0')
    const month = date.getMonth().toString().padStart(2, '') + 1
    const year = date.getFullYear().toString()
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')
    return `${day}/${month}/${year} ${hours}:${minutes}:${seconds} น.`
  }
  const lastId = ref(12)

  function checkIn() {
    const newChk = <CheckInOut>{
      id: lastId.value++,
      user: authStore.userLogin.currentUser.fullName,
      userId: authStore.userLogin.currentUser.id,
      chkIn: formatTime(new Date()),
      chkOut: '',
      totalTime: '',
      inTime: new Date().getTime(),
      outTime: 0
    }
    chkData.value.push(newChk)
    lastId.value++
  }

  function formatTotalTime(totalTime: number): string {
    const hours = Math.floor(totalTime)
    const minutes = Math.round((totalTime % 1) * 60)
    return `${hours} ชั่วโมง ${minutes} นาที`
  }

  function checkOut(chk: CheckInOut) {
    const index = chkData.value.findIndex((item) => item === chk)
    if (index !== -1) {
      console.log('C')
      chkData.value[index].chkOut = formatTime(new Date())
      chkData.value[index].outTime = new Date().getTime()
      const totalTimeMilliseconds = chkData.value[index].outTime - chkData.value[index].inTime
      const totalTimeHours = totalTimeMilliseconds / (1000 * 60 * 60)

      // Set totalTime with formatted time
      chkData.value[index].totalTime = formatTotalTime(totalTimeHours)

      console.log('CheckOut laew ja')
      console.log(chkData.value[index])
    }
  }


  return { chkData, checkIn, lastId, checkOut, formatTotalTime }
})
