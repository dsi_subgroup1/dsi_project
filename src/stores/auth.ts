import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import {  type User, type Role } from '@/types/User'
import { useUserStore } from './user';

export const useAuthStore = defineStore('auth', () => {
  const userStore = useUserStore()
  const userLogin = {
    currentUser: ref(userStore.defaultUser),
    users: ref(userStore.users)
  }

  const methods = {
    authenticateUser(): boolean {
      return userLogin.currentUser.value.id > -1;
    },
    grantAccess(routeRoles: Role[]): boolean {
      return routeRoles.every(role => userLogin.currentUser.value.roles.includes(role));
    },
    setCurrentUser(newUser: User): void {
      userLogin.currentUser.value = newUser;
      const parsed = JSON.stringify(newUser);
      localStorage.setItem('user', parsed);
    },
    checkUser(email: string, password: string): boolean {
      const foundUser = userLogin.users.value.find(user => user.email === email && user.password === password);

      if (foundUser) {
        this.setCurrentUser(foundUser);
        return true;
      }
      return false;
    },
    getCurrentUser(): User {
      return { ...userLogin.currentUser.value };
    },
    clearUser() {
      localStorage.removeItem('user');
    }
  };

  return { userLogin, methods };
});