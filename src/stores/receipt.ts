import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { usePromotionStore } from './promotion'

export const useReceiptStore = defineStore('receipt', () => {
  const receiptItems = ref<ReceiptItem[]>([])
  const authStore = useAuthStore()
  const receiptDialog = ref(false)
  const memberStore = useMemberStore()
  const point = ref(0)
  const receiveMoney = ref(0)
  const usePoint =ref(0)
  watch(usePoint,(newValue)=>{
    point.value = usePoint.value
    
  })

  const receiptDatas = ref<Receipt[]>([])
  const selectedPayment = ref('')
const selectPaymentType=()=>{
    
  }
    watch(selectedPayment, (newValue) => {
    selectedPayment.value = newValue
    if (selectedPayment.value === 'Promptpay') {
      ppSelected.value = 1
  } else if(selectedPayment.value==='Cash' ){
      // If it's not "Promptpay," you might want to reset ppSelected to another value
      ppSelected.value = 0
      
  }
    
    
})
  const receipt = ref<Receipt>({
    id: 0,
    createDate: new Date(),
    createdTimestamp: computed(() => new Date().getTime()).value,
    totalBefore: 0,
    memberDiscount: 0,
    total: 0,
    point: 0,
    receiveAmount: 0,
    change: 0,
    memberPoint: 0,
    paymentType: 'Cash',
    userId: authStore.userLogin.currentUser.id,
    user: authStore.userLogin.currentUser,
    memberId: 0
  })

  const ppSelected = ref(0)
  const ppDialog = ref(false)
  const openPromptPay = () => {
    ppDialog.value = true
    ppMoneyPay.value = receipt.value.total
  }
  const ppMoneyPay = ref(0)

  function seeReceiptHistory(receiptHistory: Receipt) {
    receipt.value = receiptHistory
    if (receiptHistory.receiptItems !== undefined) receiptItems.value = receiptHistory.receiptItems
    receiptDialog.value = true
  }

  watch(receiveMoney, (newValue) => {
    receipt.value.receiveAmount = newValue
  })
  watch(point, (newValue) => {
    receipt.value.point = newValue
  })


  function showReceiptDialog() {
    console.log("โง่"+ppSelected.value)
    if(ppSelected.value===1){
      receipt.value.paymentType='Promptpay'
    }
    console.log(point.value+"kwai")
    if (memberStore.currentMember && point.value !== undefined) {
      memberStore.currentMember.point = (memberStore.currentMember.point ?? 0) + point.value
      
      
      
    }
    if (receipt.value.member !== undefined) receipt.value.memberPoint = receipt.value.member?.point

    receipt.value.receiptItems = receiptItems.value
    receipt.value.id = lastId.value++
    
    receiptDatas.value.push(receipt.value)

    receiptDialog.value = true
    promotionStore.promotionSelected = ''
    ppSelected.value=0
  }

  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createDate: new Date(),
      createdTimestamp: computed(() => new Date().getTime()).value,
      totalBefore: 0,
      memberDiscount: 0,
      total: 0,
      point: 0,
      receiveAmount: receiveMoney.value,
      change: 0,
      memberPoint: 0,
      paymentType: 'Cash',
      userId: authStore.userLogin.currentUser.id,
      user: authStore.userLogin.currentUser,
      memberId: 0
    }
    ppSelected.value=0
    point.value = 0
    pointCurrentMember.value = 0
    memberStore.clear()
    receiveMoney.value = 0
    ppMoneyPay.value = 0
    promotionStore.promotionSelected = ''
    promotionStore.discount = 1
    promotionStore.pointDelete = 0
    promotionStore.canUsePromotion = false
    usePoint.value = 0
    
    
  }


  const lastId = ref(0)

  function addReceiptItem(product: Product) {
    point.value++
    let subCategory = 'เย็น'
    let sweetLevel = 'หวานปานกลาง'
    if (product.subCategory === 2) (subCategory = 'ร้อน'), calReceipt()
    else if (product.subCategory === 3) (subCategory = 'ปั่น'), calReceipt()

    if (product.sweetLevel === 1) (sweetLevel = 'หวานน้อย'), calReceipt()
    else if (product.sweetLevel === 3) (sweetLevel = 'หวานมาก'), calReceipt()

    if (product.subCategory === 2) (sweetLevel = '-'), calReceipt()

    if (product.type === 3) (subCategory = '-'), (sweetLevel = '-'), calReceipt()
    const index = receiptItems.value.findIndex(
      (item) =>
        item.product?.id === product.id &&
        item.product.type === product.type &&
        item.slcSweetLvl === product.sweetLevel &&
        item.slcSubCate === product.subCategory
    )

    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    }

    const newReceipt: ReceiptItem = {
      id: -1,
      name: product.name,
      subCategory: subCategory,
      sweetLevel: sweetLevel,
      price: product.price,
      unit: 1,
      productId: product.id,
      product: product,
      slcSubCate: product.subCategory,
      slcSweetLvl: product.sweetLevel
    }
    receiptItems.value.push(newReceipt)

    console.log(receiptDatas.value)
    calReceipt()
  }

  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    const removePoint = receiptItem.unit
    point.value = point.value - removePoint
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }
  const pointCurrentMember = ref(0)
  function calReceipt() {
    
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.price * item.unit
    }
    receipt.value.totalBefore = totalBefore
    if (memberStore.currentMember) {
      
      

      if (point.value > 1) {
        if (promotionStore.promotionSelected === '10 Point Get 1 Free') {
          alert('จำนวนสินค้ามากเกินกำหนด')
          clear()
          promotionStore.promotionSelected = ''
          promotionStore.discount = 1
          point.value = 0
        } else if(promotionStore.promotionSelected === '20 Point Get 10% Discount'){
          usePoint.value = -20
          receipt.value.total = totalBefore * promotionStore.discount
          receipt.value.memberDiscount = receipt.value.totalBefore - receipt.value.total
        }
      
      }else if(promotionStore.promotionSelected==='10 Point Get 1 Free'){
        receipt.value.total = totalBefore * promotionStore.discount
        usePoint.value = -10
        receipt.value.memberDiscount = receipt.value.totalBefore - receipt.value.total
      }

      pointCurrentMember.value = memberStore.currentMember.point
    }
    // โปรโมชั่น
    else receipt.value.total = totalBefore
  }
  const promotionStore = usePromotionStore()


  return {
    receiptItems,
    receipt,
    receiptDialog,
    point,
    receiveMoney,
    pointCurrentMember,
    receiptDatas,
    ppDialog,
    ppSelected,
    ppMoneyPay,
    selectedPayment,
    showReceiptDialog,
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    calReceipt,
    clear,
    seeReceiptHistory,
    openPromptPay
  }
})
