import { type Product } from "@/types/Product"

const products1: Product[] = [
  { id: 1, name: 'ลาเต้', price: 35.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 2, name: 'มอคค่า', price: 45.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 3, name: 'คาปูชิโน่', price: 55.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 4, name: 'เอสเปรสโซ่', price: 45.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 5, name: 'อเมริกาโน่', price: 55.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 6, name: 'กาแฟส้ม', price: 45.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 7, name: 'คาราเมล', price: 55.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 8, name: 'ช็อคโกแลต', price: 45.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 9, name: 'เอสเพรสโซ่ มัคคิอาโต', price: 55.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 10, name: 'คาราเมล มัคคิอาโต', price: 45.0, type: 1, subCategory: 0, sweetLevel: 0 },
  { id: 11, name: 'กาแฟ', price: 55.0, type: 1, subCategory: 0, sweetLevel: 0 }
]

const products2: Product[] = [
  { id: 1, name: 'ชาเขียว', price: 35.0, type: 2, subCategory: 0, sweetLevel: 0 },
  { id: 2, name: 'ชาไทย', price: 45.0, type: 2, subCategory: 0, sweetLevel: 0 },
  { id: 3, name: 'ชานม', price: 45.0, type: 2, subCategory: 0, sweetLevel: 0 },
  { id: 4, name: 'ชามะนาว', price: 45.0, type: 2, subCategory: 0, sweetLevel: 0 },
  { id: 5, name: 'ชาผลไม้', price: 45.0, type: 2, subCategory: 0, sweetLevel: 0 }
]

const products3: Product[] = [
  { id: 1, name: 'เค้ก', price: 35.0, type: 3, subCategory: 0, sweetLevel: 0 },
  { id: 2, name: 'โทสต์', price: 45.0, type: 3, subCategory: 0, sweetLevel: 0 },
  { id: 3, name: 'วัฟเฟิล', price: 55.0, type: 3, subCategory: 0, sweetLevel: 0 },
  { id: 4, name: 'ชีสเค้ก', price: 55.0, type: 3, subCategory: 0, sweetLevel: 0 }
]
const products4: Product[] = [...products1, ...products2, ...products3];

export { products1, products2, products3, products4 }