type Product = {
  id: number
  name: string 
  price: number
  type: number // 1Coffee 2Tea 3Cake
  subCategory: number// 1cold 2hot 3Frappe
  sweetLevel: number
}


export {type Product }
