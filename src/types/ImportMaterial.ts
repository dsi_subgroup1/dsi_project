type ImportMaterial = {
  id: number
  employee: string
  vendor: string
  Rdate: string
  Rtotallist: number
  Rtotal: number
  details: {
      materialid: number
      no: number,
      material: String,
      exDate: String,
      quantity: number
      unitsprice: number
      price: number
  }[];
}
  
  export type {ImportMaterial}
  