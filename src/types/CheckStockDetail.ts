type CheckStockDetail = {
    id:number
    checkStockId:number
    materialId:number
    material:string
    checkLast:number
    checkRemain:number
}
export {type CheckStockDetail}