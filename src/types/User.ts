
type Role = 'Manager' | 'Employee';

interface User {
    id: number;
    email: string;
    password: string;
    fullName: string;
    roles: Role[];
    salary: string;
}


export { type Role, type User }