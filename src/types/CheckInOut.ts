type CheckInOut = {
    id:number
    user:string
    userId:number
    chkIn:string
    chkOut:string
    totalTime:string
    inTime:number
    outTime:number
}
export type{CheckInOut}