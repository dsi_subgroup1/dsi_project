type Promotion = {
    id:number
    name:string
    startDate:string
    endDate:String
    usePoint:number
    status:number
    //1 on 2 off
    discount:number

}

export type{Promotion}