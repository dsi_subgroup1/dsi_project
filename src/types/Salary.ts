type Role = 'Manager' | 'Employee';

interface Salary {
    id: number;
    email: string;
    password: string;
    fullName: string;
    roles: Role[];
    salary: string;
    status: string;
    date: string;
}


export { type Role, type Salary }