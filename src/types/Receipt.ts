import type {User} from './User'
import type {Member} from './Member'
import type { ReceiptItem } from './ReceiptItem';

type Receipt = {
    id: number;
    createDate: Date;
    createdTimestamp: number    
    totalBefore:number,
    memberDiscount:number,
    total: number;
    receiveAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?:Member
    memberPoint:number
    point:number
    receiptItems?:ReceiptItem[]
}

export type {Receipt}
