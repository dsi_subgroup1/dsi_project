import type { Product } from "./Product";

type ReceiptItem={
    id: number;
    name: string;
    slcSubCate:number
    subCategory:string;
    slcSweetLvl:number
    sweetLevel:string;
    price: number;
    unit: number;
    productId: number;
    product?: Product;
}

export {type ReceiptItem}
