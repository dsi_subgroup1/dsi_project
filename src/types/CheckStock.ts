import type { CheckStockDetail } from "./CheckStockDetail"
type CheckStock = {
    id:number
    user:string
    date:string
    time:string
    details:CheckStockDetail[]
}
export {type CheckStock}